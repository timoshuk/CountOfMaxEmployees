package by.timoshuk;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmEmployee {

    public static int getMaxNumberEmployees(List<Employee> employeeList) {
        int maxNumberEmployees = 0;
        int counterOfWorkingEmployees = 0;
        List <Employee> workingEmployeesList = new ArrayList<>();

        for (int i = 0; i < employeeList.size(); i++) {
            workingEmployeesList.add(employeeList.get(i));
            counterOfWorkingEmployees ++;
            for (int j = 0; j < workingEmployeesList.size(); j++) {
                if(workingEmployeesList.get(j).getTimeBeginMin() + workingEmployeesList.get(j).getWorkingTimeMin() < employeeList.get(i).getTimeBeginMin()){
                    workingEmployeesList.remove(workingEmployeesList.get(j));
                    counterOfWorkingEmployees --;
                }
            }
            if (counterOfWorkingEmployees > maxNumberEmployees){
                maxNumberEmployees = counterOfWorkingEmployees;
            }
        }
        return maxNumberEmployees;

    }

    public static List<Employee> createListOfEmployees(String timeOfEmploees) {
        String [] timeOfEachEmpoee = timeOfEmploees.split("\\n");
        List <Employee> emploeeList = new ArrayList<>();
        int numberOfEmployee = 1;
        String timeBeginWork ;
        String timeFinishWork;

        for (String s : timeOfEachEmpoee) {
            if (s.matches("\\d{1,2}\\:\\d{2}\\ \\d{1,2}\\:\\d{2}")){
                timeBeginWork = s.substring(0,s.indexOf(' '));
                timeFinishWork = s.substring(s.indexOf(' ')+1) ;
                Employee employee = new Employee("Employee " + numberOfEmployee, timeBeginWork, timeFinishWork );

                employee.setTimeBeginMin(Integer.parseInt(timeBeginWork.substring(0, timeBeginWork.indexOf(':'))) * 60 + Integer.parseInt(timeBeginWork.substring( timeBeginWork.indexOf(':')+1)));
                employee.setWorkingTimeMin(Integer.parseInt(timeFinishWork.substring(0, timeFinishWork.indexOf(':'))) * 60 + Integer.parseInt(timeFinishWork.substring( timeFinishWork.indexOf(':')+1)) - employee.getTimeBeginMin());
                emploeeList.add(employee);
            }else {
                System.out.println("Sorry, you have a mistake in the file input.txt");
            }
            numberOfEmployee++;
        }
        return emploeeList;
    }

    public static void sortListOfEmployees(List<Employee> emploeeList) {
        emploeeList.sort((a, b) -> Integer.compare(a.getTimeBeginMin(), b.getTimeBeginMin()));
    }
}
