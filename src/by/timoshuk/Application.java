package by.timoshuk;

import java.util.*;

public class Application {

    private String fileName = "input.txt";
    private List <Employee> emploeeList;

    private Application() {
    }

    public static void main(String[] args) {
        Application app = new Application();
        app.runTheProgramm();
    }

    private void runTheProgramm() {
        String timeOfEmploees = ReadFile.readTimeFromFile(fileName);
        emploeeList = AlgorithmEmployee.createListOfEmployees(timeOfEmploees);
        AlgorithmEmployee.sortListOfEmployees(emploeeList);
        System.out.println("Ответ: " + AlgorithmEmployee.getMaxNumberEmployees(emploeeList));
    }
}