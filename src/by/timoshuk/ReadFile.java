package by.timoshuk;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

    public static String readTimeFromFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder("");
        BufferedReader br = null;
        String [] timeOfEmploees ;
        try {
            br = new BufferedReader(new FileReader(fileName));
            String tmp = "";
            while ((tmp = br.readLine()) != null) {
                timeOfEmploees = tmp.split("\n");
                for (String res : timeOfEmploees) {
                    stringBuilder.append(res).append("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }
}
