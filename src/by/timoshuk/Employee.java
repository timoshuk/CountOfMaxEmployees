package by.timoshuk;

import java.util.Objects;

public class Employee {

    private String name;

    private String timeBeginWork;
    private String timeFinishWork;
    private int timeBeginMin;
    private int workingTimeMin;

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", timeBeginWork='" + timeBeginWork + '\'' +
                ", timeFinishWork='" + timeFinishWork + '\'' +
                ", timeBeginMin=" + timeBeginMin +
                ", workingTimeMin=" + workingTimeMin +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return timeBeginMin == employee.timeBeginMin &&
                workingTimeMin == employee.workingTimeMin &&
                Objects.equals(name, employee.name) &&
                Objects.equals(timeBeginWork, employee.timeBeginWork) &&
                Objects.equals(timeFinishWork, employee.timeFinishWork);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, timeBeginWork, timeFinishWork, timeBeginMin, workingTimeMin);
    }

    public Employee(String name, String timeBeginWork, String timeFinishWork) {
        this.name = name;
        this.timeBeginWork = timeBeginWork;
        this.timeFinishWork = timeFinishWork;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeBeginWork() {
        return timeBeginWork;
    }

    public void setTimeBeginWork(String timeBeginWork) {
        this.timeBeginWork = timeBeginWork;
    }

    public String getTimeFinishWork() {
        return timeFinishWork;
    }

    public void setTimeFinishWork(String timeFinishWork) {
        this.timeFinishWork = timeFinishWork;
    }

    public int getTimeBeginMin() {
        return timeBeginMin;
    }

    public void setTimeBeginMin(int timeBeginMin) {
        this.timeBeginMin = timeBeginMin;
    }

    public int getWorkingTimeMin() {
        return workingTimeMin;
    }

    public void setWorkingTimeMin(int workingHours) {
        this.workingTimeMin = workingHours;
    }
}
