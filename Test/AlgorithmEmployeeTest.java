import by.timoshuk.AlgorithmEmployee;
import by.timoshuk.Employee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AlgorithmEmployeeTest {

    @Test
    public void createListOfEmployeesTest(){

        String time = "09:00 10:10\n" +
            "08:40 11:30\n" +
            "10:20 13:50";
        List<Employee> employeeList = AlgorithmEmployee.createListOfEmployees(time);
        List <Employee> expectedEmployeeList = new ArrayList<>();

        Employee employee1 = new Employee("Employee 1", "09:00", "10:10");
        employee1.setTimeBeginMin(540);
        employee1.setWorkingTimeMin(70);
        expectedEmployeeList.add(employee1);

        Employee employee2 = new Employee("Employee 2", "08:40", "11:30");
        employee2.setTimeBeginMin(520);
        employee2.setWorkingTimeMin(170);
        expectedEmployeeList.add(employee2);

        Employee employee3 = new Employee("Employee 3", "10:20", "13:50");
        employee3.setTimeBeginMin(620);
        employee3.setWorkingTimeMin(210);
        expectedEmployeeList.add(employee3);

        assertEquals(expectedEmployeeList, employeeList);
    }

    @Test
    public void getMaxNumberEmployees(){
        List <Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee("Employee 1", "09:00", "10:10");
        employee1.setTimeBeginMin(540);
        employee1.setWorkingTimeMin(70);
        employeeList.add(employee1);

        Employee employee2 = new Employee("Employee 2", "08:40", "11:30");
        employee2.setTimeBeginMin(520);
        employee2.setWorkingTimeMin(170);
        employeeList.add(employee2);

        Employee employee3 = new Employee("Employee 3", "10:20", "13:50");
        employee3.setTimeBeginMin(620);
        employee3.setWorkingTimeMin(210);
        employeeList.add(employee3);

        int maxNumber = AlgorithmEmployee.getMaxNumberEmployees(employeeList);
        int expectedMaxNumber = 2;
        assertEquals(expectedMaxNumber, maxNumber);
    }

    @Test
    public void sortListOfEmployeesTest() {
        List<Employee> expectedEmployeeList = new ArrayList<>();
        List<Employee> employeeList = new ArrayList<>();
        Employee employee1 = new Employee("Employee 1", "09:00", "10:10");
        employee1.setTimeBeginMin(540);
        employee1.setWorkingTimeMin(70);

        Employee employee2 = new Employee("Employee 2", "08:40", "11:30");
        employee2.setTimeBeginMin(520);
        employee2.setWorkingTimeMin(170);

        Employee employee3 = new Employee("Employee 3", "10:20", "13:50");
        employee3.setTimeBeginMin(620);
        employee3.setWorkingTimeMin(210);
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);

        expectedEmployeeList.add(employee2);
        expectedEmployeeList.add(employee1);
        expectedEmployeeList.add(employee3);

        AlgorithmEmployee.sortListOfEmployees(employeeList);
        assertEquals(expectedEmployeeList, employeeList);
    }
}


